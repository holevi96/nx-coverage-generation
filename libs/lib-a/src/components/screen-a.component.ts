import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'lib-screen-a',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './screen-a.component.html',
  styleUrl: './screen-a.component.css',
})
export class ScreenAComponent {}
