import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'lib-screen-b',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './screen-b.component.html',
  styleUrl: './screen-b.component.css',
})
export class ScreenBComponent {}
