/* eslint-disable */
export default {
  displayName: 'lib-b',
  preset: '../../jest.preset.js',
  setupFilesAfterEnv: ['<rootDir>/src/test-setup.ts'],
  transform: {
    '^.+\\.(ts|mjs|js|html)$': [
      'jest-preset-angular',
      {
        tsconfig: '<rootDir>/tsconfig.spec.json',
        stringifyContentPathRegex: '\\.(html|svg)$',
      },
    ],
  },
  transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
  snapshotSerializers: [
    'jest-preset-angular/build/serializers/no-ng-attributes',
    'jest-preset-angular/build/serializers/ng-snapshot',
    'jest-preset-angular/build/serializers/html-comment',
  ],
  collectCoverage: true,
  coverageDirectory: '../../coverage/libs/lib-a',
  coverageReporters: ['cobertura', 'json', 'lcov', 'text', 'text-summary'],
  reporters: [
    'default',
    ['jest-junit', { outputDirectory: 'junit-reports', outputName: `test-lib-b-${Date.now()}.xml` }],
    ['jest-sonar', { outputDirectory: 'sonar-reports', outputName: `test-lib-b-${Date.now()}.xml` }],
  ],
  collectCoverageFrom: [
    '<rootDir>/src/**/*.ts',
    '!<rootDir>/src/**/*.module.ts',
    '!<rootDir>/src/**/index.ts',
    '!<rootDir>/src/**/*.stories.ts',
  ],
};
