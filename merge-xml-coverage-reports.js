const glob = require('glob');
const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');

const getXmlFiles = function (src) {
  return new Promise(resolve => {
    glob(`${src}/**/cobertura-coverage.xml`, (error, result) => {
      if (error) resolve([]);
      resolve(result);
    });
  });
};

(async function () {
  const files = await getXmlFiles('coverage');
  const packages = files.map((f, i) => `package${i + 1}=${f}`).join(' ');

  const script = `npx cobertura-merge -o coverage/merged-cobertura-coverage.xml ${packages}`;

  exec(script, {
    stdio: [0, 1, 2],
    cwd: __dirname,
  });
})();
